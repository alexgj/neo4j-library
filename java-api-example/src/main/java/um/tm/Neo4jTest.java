package um.tm;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.neo4j.graphalgo.GraphAlgoFactory;
import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.PathExpander;
import org.neo4j.graphdb.PathExpanders;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

/**
 * Esta clases muestra un ejemplo b�sico de uso de la base de datos no
 * relacional Neo4j.
 * 
 * Se puede encontrar m�s informaci�n en el siguiente enlace:
 * http://neo4j.com/developer/java/
 * 
 * Tambi�n recomiendo la lectura del cap�tulo 33 del manual de Neo4j:
 * http://neo4j.com/docs/stable/tutorials-java-embedded.html
 * 
 * @author Manuel
 *
 */
public class Neo4jTest {

	public enum RelTypes implements RelationshipType {
		REL_TYPE_1, REL_TYPE_2
	}
	
	public static final String NOMBRE_NODO = "MyNode";
	public static final String NOMBRE_ATRIBUTO_ID = "id";
	public static final String NOMBRE_ATRIBUTO_DESCRIPCION = "desc";
	public static final Label labelMyNode = DynamicLabel.label(NOMBRE_NODO);

	public static void main(String[] args) throws IOException {

		long iTime, fTime;
		Logger.getAnonymousLogger().info("Building Neo4j graph from asserted ontology model...");
		iTime = System.currentTimeMillis();

		// STEP 1: CREATE THE GRAPH DATABASE IN A TEMPORARY FOLDER
		File tmpDir = File.createTempFile("neo4j", ".db");
		tmpDir.delete();
		tmpDir.mkdir();
		GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(tmpDir);	
		
		// STEP 2: CREATE A UNIQUENESS CONSTRAINT FOR THE PROPERTY ID
		try (Transaction tx = graphDb.beginTx()) {
			graphDb.schema().constraintFor(labelMyNode).assertPropertyIsUnique(NOMBRE_ATRIBUTO_ID).create();
			tx.success();
		} catch (Exception ex) {
			throw ex;
		}

		// STEP 4: CREATE 4 NODES WITH TWO PROPERTIES (ID AND DESCRIPTION)
		// "A", "Nodo A"
		Node nodeA = createMyNode(graphDb, "A", "Nodo A");
		System.out.println(toString(graphDb, nodeA));
		// "B", "Nodo B"
		Node nodeB = createMyNode(graphDb, "B", "Nodo B");
		System.out.println(toString(graphDb, nodeB));
		// "C", "Nodo C"
		Node nodeC = createMyNode(graphDb, "C", "Nodo C");
		System.out.println(toString(graphDb, nodeC));
		// "D", "Nodo D"
		Node nodeD = createMyNode(graphDb, "D", "Nodo D");
		System.out.println(toString(graphDb, nodeD));
		
		// STEP 5: CREATE A NODE WITH THE SAME ID THAN ANOTHER PREVIOUSLY CREATED (CONSTRAINT VIOLATION)
		try {
			Node nodeARep = createMyNode(graphDb, "A", "Nodo A");
			System.out.println(toString(graphDb, nodeARep));
		}
		catch ( Exception ex ) {
			System.out.println(ex.getMessage());
		}

		// STEP 6: CREATE THE RELATIONS OF THE TYPE 1 BETWEEN NODES
		createLinkRel(graphDb, RelTypes.REL_TYPE_1, nodeA, nodeB);
		createLinkRel(graphDb, RelTypes.REL_TYPE_1, nodeB, nodeC);
		createLinkRel(graphDb, RelTypes.REL_TYPE_1, nodeC, nodeD);
		createLinkRel(graphDb, RelTypes.REL_TYPE_1, nodeA, nodeD);
		
		// STEP 7: CREATE THE RELATIONS OF THE TYPE 2 BETWEEN NODES
		createLinkRel(graphDb, RelTypes.REL_TYPE_2, nodeA, nodeC);
		createLinkRel(graphDb, RelTypes.REL_TYPE_2, nodeC, nodeB);
		createLinkRel(graphDb, RelTypes.REL_TYPE_2, nodeB, nodeD);
		createLinkRel(graphDb, RelTypes.REL_TYPE_2, nodeD, nodeA);
		
		// STEP 8: ALGORITHMS DALING WITH PATH. 
		PathExpander<Object> expander1 = getPathExpander(RelTypes.REL_TYPE_1);
		PathFinder<Path> finder1 = GraphAlgoFactory.shortestPath(expander1, 100/*maxDepth*/);
		//PathFinder<Path> finder1 = GraphAlgoFactory.allPaths(expander1, 100/*maxDepth*/);
		Transaction tx = null;
		try {
			tx = graphDb.beginTx();
			System.out.println("Searching paths from Node="+nodeA.getId()+" to Node="+nodeD.getId());
			Iterable<Path> paths = finder1.findAllPaths(nodeA, nodeD);
			int i = 0;
			for ( Path path : paths ) {
				System.out.println("\t"+i+": "+path.toString());
				i++;
			}
		} catch (Exception ex) {
			throw ex;
		}
		finally {
			if ( tx != null ) tx.close();
		}
		
		// STEP 8: ALGORITHMS DALING WITH PATH. 
		PathExpander<Object> expander2 = getPathExpander(RelTypes.REL_TYPE_2);
		PathFinder<Path> finder2 = GraphAlgoFactory.shortestPath(expander2, 100/*maxDepth*/);
		//PathFinder<Path> finder2 = GraphAlgoFactory.allPaths(expander2, 100/*maxDepth*/);
		try {
			tx = graphDb.beginTx();
			System.out.println("Searching paths from Node="+nodeA.getId()+" to Node="+nodeD.getId());
			Iterable<Path> paths = finder2.findAllPaths(nodeA, nodeD);
			int i = 0;
			for ( Path path : paths ) {
				System.out.println("\t"+i+": "+path.toString());
				i++;
			}
		} catch (Exception ex) {
			throw ex;
		}
		finally {
			if ( tx != null ) tx.close();
		}	
		
		fTime = System.currentTimeMillis();
		Logger.getAnonymousLogger().info("Graph builded in " + (fTime - iTime) + " msec");
		// Logger.getAnonymousLogger().info("Number of nodes: "+factory.getNumberOfNodes());
		// Logger.getAnonymousLogger().info("Number of relationships: "+factory.getNumberOfRelationships());

	}

	private static Node createMyNode(GraphDatabaseService graphDb, String nodeId, String nodeDesc) {
		Transaction tx = null;
		try {
			tx = graphDb.beginTx();
			Node node = graphDb.createNode(labelMyNode);
			node.setProperty(NOMBRE_ATRIBUTO_ID, nodeId);
			node.setProperty(NOMBRE_ATRIBUTO_DESCRIPCION, nodeDesc);
			tx.success();
			return node;
		} catch (Exception ex) {
			throw ex;
		}
		finally {
			if ( tx != null ) tx.close();
		}
	}
	
	private static void createLinkRel (GraphDatabaseService graphDb, RelTypes type, Node node1, Node node2 ) {
		Transaction tx = null;
		try {
			tx = graphDb.beginTx();
			node1.createRelationshipTo(node2, type);
			System.out.println("Relation \""+type+"\" between Node="+node1.getId()+" and Node ="+node2.getId()+" created.");
			tx.success();
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
		finally {
			if ( tx != null ) tx.close();
		}
	}

	private static String toString(GraphDatabaseService graphDb, Node n) {
		Transaction tx = null;
		try {
			tx = graphDb.beginTx();
			String toString = "";
			toString += n.getId() + ", ";
			toString += n.getLabels() + ", ";
			for (String propKey : n.getPropertyKeys()) {
				toString += "[" + propKey + "=" + n.getProperty(propKey) + "]"+ ", ";
			}
			if (toString.endsWith(", ")) {
				toString = toString.substring(0, toString.length()-(", ".length()));
			}
			return toString;
		} catch (Exception ex) {
			throw ex;
		}
		finally {
			if ( tx != null ) tx.close();
		}
	}
	
	private static PathExpander<Object> getPathExpander(RelTypes type) {
		// FOR EACH TYPE OF SEARCH PATHEXPANDER MUST BE DEFINE.
		PathExpander<Object> expander;
		/* THE EDGES ARE BIDIRECTION, IN THE EXPANDER YOU DECIDE WHICH ONE TO USE*/
		expander = PathExpanders.forTypeAndDirection(type, Direction.OUTGOING);  
		return expander;
	}

}
