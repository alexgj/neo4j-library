# README

Library with functionality using the Neo4j api.

# PROJECTS

1. **java-api-example**: brief and simple example about how to use Neo4j Java API.
2. **owl-in-neo4j**: map an OWL ontology into a Neo4j directory database. Create nodes for OWL classes and relations based on the inferred is-a relations. Finally, some basic queries are executed over the Neo4j inferred hierarchycal model.

# ECLIPSE DEPLOYMENT

Both Projects are independent Maven Projects. Imports them independtly in Eclipse. Set as Project type "Maven" -> "Existing Maven Projects". Browse the directory that contains the "pom.xml".

# Testing

1. Compile the project

2. Choose the ontology editing the file */owl-in-neo4j/src/main/resources/config.properties*

3. Run the test

	`mvn test -Dtest=Test_Paths`

	This will generate a folder (neo4j.db) which is the neo4j database.

4. Go to your neo4j installation and modify the file *[neo4jInstallationDir]/conf/neo4j.conf*

	`#The name of the database to mount`
	`dbms.active_database=neo4j.db`

5. Copy the neo4j.db folder to *[neo4jInstallationDir]/data/databases*

6. Restart neo4j
