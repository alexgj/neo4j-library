package um.owlapi2neo4jgraph;

import java.util.logging.Logger;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;

import um.owlapi2neo4jgraph.relationships.expanders.OwlClassExpander;

public class Neo4jGraphedOntology {

	private final OWLOntology ontology;
	private final Neo4jGraphedOntolgoyFactory factory;
	
	public Neo4jGraphedOntology (OWLOntology ontology, OwlClassExpander... expanders) {
		
		long iTime, fTime;
		Logger.getAnonymousLogger().info("Building Neo4j graph from ontology model...");
		iTime = System.currentTimeMillis();
		
		// Store the ontology
		this.ontology = ontology;

		// Create a graph data factory
		this.factory = new Neo4jGraphedOntolgoyFactory();
			
		// Obtain the Thing class
		OWLClass thing = ontology.getOWLOntologyManager().getOWLDataFactory().getOWLThing();
			
		// Create a node in the graph of Thing class
		this.factory.createNode(thing);
			
		// Expand the Thing class creating relations within the graph
		for ( OwlClassExpander expander : expanders ) {
			expander.expand(factory, thing);
		}
		
		factory.closeBatch();
		
		fTime = System.currentTimeMillis();
		Logger.getAnonymousLogger().info("Graph builded in "+(fTime-iTime)+" msec");
		Logger.getAnonymousLogger().info("Number of nodes: "+factory.getNumberOfNodes());
		Logger.getAnonymousLogger().info("Number of relationships: "+factory.getNumberOfRelationships());

	}
	
	// GETTERS AND SETTERS
	
	public OWLOntology getOntology() {
		return ontology;
	}
	
	public Neo4jGraphedOntolgoyFactory getFactory() {
		return factory;
	}
	
}
