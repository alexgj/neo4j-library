package um.owlapi2neo4jgraph;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {
	
	private static Configuration instance = null;
	
	private Properties properties =  new Properties();
	
	private Configuration(){
		
		InputStream input = null;
		
		try {			
			input = Configuration.class.getClassLoader().getResourceAsStream("config.properties");			
			this.properties.load(input);
		} catch (IOException ex) {	
			ex.printStackTrace();
		}finally{
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}			
		}		
	}
	
	public static synchronized Configuration getInstance(){
		if (instance == null)
			instance = new Configuration();
		return instance;
	}
	
	public String getValue(String propKey){
		return this.properties.getProperty(propKey);
    }
}
