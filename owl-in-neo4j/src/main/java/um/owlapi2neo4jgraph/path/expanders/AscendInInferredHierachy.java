package um.owlapi2neo4jgraph.path.expanders;

import org.neo4j.graphalgo.GraphAlgoFactory;
import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.PathExpander;
import org.neo4j.graphdb.PathExpanders;

import um.owlapi2neo4jgraph.Neo4jGraphedOntolgoyFactory;

public class AscendInInferredHierachy {

	public static PathFinder<Path> getPathFinder(Integer maxDepth) {
		// Create a path finder that expand by super class patterns
		PathExpander<Object> expander;
		expander = PathExpanders.forTypeAndDirection(
				Neo4jGraphedOntolgoyFactory.RelTypes.INFERRED_CHILD, 
				Direction.INCOMING);
		PathFinder<Path> finder = GraphAlgoFactory.allPaths(expander, maxDepth);
		return finder;
	}
	
}
