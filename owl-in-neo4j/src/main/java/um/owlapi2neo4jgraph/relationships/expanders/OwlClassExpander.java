package um.owlapi2neo4jgraph.relationships.expanders;

import org.semanticweb.owlapi.model.OWLClass;

import um.owlapi2neo4jgraph.Neo4jGraphedOntolgoyFactory;

public interface OwlClassExpander {

	public void expand(Neo4jGraphedOntolgoyFactory factory, OWLClass owlClass);
	
}
