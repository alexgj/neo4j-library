package um.owlapi2neo4jgraph.relationships.expanders;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.neo4j.graphdb.Node;

import org.neo4j.graphdb.RelationshipType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;

import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import um.owlapi2neo4jgraph.Neo4jGraphedOntolgoyFactory;

public class OwlClassExpander_InferredSubclasses implements OwlClassExpander {
	
	private OWLReasoner owlReasoner;
	private HashSet<Node> opened;
	
	public OwlClassExpander_InferredSubclasses(OWLReasoner reasoner) {
		this.owlReasoner = reasoner;
		this.opened  = new HashSet<Node>();
	}
	
	@Override
	public void expand(Neo4jGraphedOntolgoyFactory factory, OWLClass owlClass) {
		
		// Obtain the original node
		Node orgNode = factory.createNode(owlClass,owlClass.getIRI().getShortForm(), null);		
		
		if ( ! opened.contains(orgNode) ) {
			opened.add(orgNode);
			//Logger.getAnonymousLogger().info("Expanding node "+opened.size()+" ("+owlClass+")...");			

			// Obtain the subclasses
			NodeSet<OWLClass> subClasses = owlReasoner.getSubClasses(owlClass, true);
			
			OWLOntology ontology = owlReasoner.getRootOntology();			
			
			createAxiomsRelationships(ontology,owlClass,factory,orgNode);

			// Create a relationship
			//int relationshipsCounter = 0;
			for ( OWLClass currentSubClass : subClasses.getFlattened() ) {

				// Retrieve the dst node or to create it if it does not exist
				String label = getLabel(currentSubClass,ontology);
				Node dstNode = factory.createNode(currentSubClass,currentSubClass.getIRI().getShortForm(), label);

				// Create the relationships
				factory.createRelationship(orgNode, dstNode, Neo4jGraphedOntolgoyFactory.RelTypes.INFERRED_CHILD);				
				//relationshipsCounter++;

				// Expand the children
				expand(factory, currentSubClass);
			}
			//Logger.getAnonymousLogger().info("Node "+owlClass+" expanded ("+relationshipsCounter+" relationships created)");
		}
		
	}	
	
	private void createAxiomsRelationships(OWLOntology ontology, OWLClass originClass, Neo4jGraphedOntolgoyFactory factory,
			Node orgNode) {
	
		Set<OWLClassAxiom> tempAx = ontology.getAxioms(originClass);
		//System.out.println("**** " + originClass.getIRI().getShortForm().toUpperCase() + " *****");
		
		String propertyName = "AXIOM";
		
		for (OWLClassAxiom ax : tempAx) {
//			System.out.println("\t####");
//			System.out.println("\t" +ax.toString());
//			System.out.println(" ");
//			System.out.print("\tCreando desde " + originClass.getIRI().getShortForm() + " a " );	
			ArrayList<OWLClass> dest = new ArrayList();
			for (OWLClassExpression nestedClassExpression : ax.getNestedClassExpressions()) {				
				
				for (OWLClass owlClass : nestedClassExpression.getClassesInSignature()) {					
					if ( !owlClass.toString().equals(originClass.toString()) && !dest.contains(owlClass) ) {
						dest.add(owlClass);						
					}
				}
			}

			for ( OWLClass dstClass : dest) {
				Node dstNode = factory.getNode(dstClass);
				if ( dstNode == null ) {
					String label = getLabel(dstClass,ontology);
					dstNode = factory.createNode(dstClass,dstClass.getIRI().getShortForm(), label);
				}
				factory.createRelationship2(orgNode, dstNode, RelationshipType.withName(propertyName), ax.toString());				
//				System.out.print(" " + dstClass.getIRI().getFragment());
			}
//			System.out.println();
//			System.out.println();
		}
	}
	
	private String getLabel(OWLClass owlClass, OWLOntology ontology) {
		
		IRI cIRI = owlClass.getIRI();
		String res = null;
		for(OWLAnnotationAssertionAxiom a : ontology.getAnnotationAssertionAxioms(cIRI)) {
		    if(a.getProperty().isLabel()) {					    	
		        if(a.getValue() instanceof OWLLiteral) {
		            OWLLiteral val = (OWLLiteral) a.getValue();
		            if ( val.hasLang("en") ) {
		            	res = val.getLiteral();
		            }
		        }
		    }
		}
		return res;
	}
}
