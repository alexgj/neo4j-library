package um.owlapi2neo4jgraph;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.index.IndexManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;

import com.google.common.collect.Iterables;

public class Neo4jGraphedOntolgoyFactory {

	private final GraphDatabaseService graphDb;
	private final HashMap<OWLClass, Node> mapIriNode;
	private final HashMap<Node, OWLClass> mapNodeIri;
	private final HashMap<Node, List<Relationship>> mapRelationships;
	
	private int numOperations = 0;
	
	private final static int BATCH_SIZE = 200000;
	
	private Transaction tx = null;

	public Neo4jGraphedOntolgoyFactory() {	
//		try {
			// Create a tmp file to save the database
//			File tmpDir = File.createTempFile("neo4j", ".db");			
//			tmpDir.delete();
//			tmpDir.mkdir();
			File tmpDir = new File("neo4j.db");
			
			// Create the GraphDatabase object
			graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(tmpDir);
			// Create a unique constraint
			try ( Transaction tx = graphDb.beginTx() )
			{
				// SOURCE: http://neo4j.com/docs/stable/tutorials-java-embedded-unique-nodes.html
				graphDb.schema().constraintFor( DynamicLabel.label( "Class" ) ).assertPropertyIsUnique( "uri" ).create();
				tx.success();
			}
			// Obtain the index manager and create the indexes
			//try ( Transaction tx = graphDb.beginTx() )
			//{
			//	// SOURCE: http://neo4j.com/docs/stable/tutorials-java-embedded-new-index.html
			//    graphDb.schema().indexFor( DynamicLabel.label( "Class" ) ).on( "uri" ).create();
			//    tx.success();
			//}
			// Create the map between classes and node ids
			this.mapIriNode = new HashMap<OWLClass, Node>();
			this.mapNodeIri = new HashMap<Node, OWLClass>();
			this.mapRelationships = new HashMap<Node,List<Relationship>>();

//		} catch (IOException ex) {
//			throw new IllegalStateException(ex.getMessage());
//		}			

	}

	// METHODS NODES OPERATIONS

	public Node createNode(OWLClass owlClass, String name, String label) {
		final String LABEL_NODE = "Class";
		if (tx == null){
			tx = graphDb.beginTx();
		}
		// Check that the node does not exist
		Node node = getNode(owlClass);
		if ( node == null ) {			
			try {
				Label l = DynamicLabel.label(LABEL_NODE);
				node = graphDb.createNode(l);

				if (name == null) {
					name = owlClass.getIRI().toURI().toString();
				}

				if ( label != null ) {
					node.setProperty("label", label);
				} 

				node.setProperty("name", name);
				node.setProperty("uri", owlClass.getIRI().toURI().toString());
				
				mapIriNode.put(owlClass, node);
				mapNodeIri.put(node, owlClass);
				//if ( ! graphDbIndex.existsForNodes( LABEL_NODE ) ) {
				//	// OPTION 1: http://neo4j.com/docs/stable/indexing-create.html
				//	graphDbIndex.forNodes(LABEL_NODE);
				//}
			    if (++numOperations % BATCH_SIZE == 0) {
			        tx.success();
			        tx.close();
			        tx = graphDb.beginTx();
			    }				
				return node;
			} catch (Exception ex) {
				throw new IllegalStateException(ex);
			}			
		}
		return node;
	}
	
	public Node createNode(OWLClass owlClass) {		
		return this.createNode(owlClass, null, null);
	}

	public Node getNode(OWLClass owlClass) {
		Node node = mapIriNode.get(owlClass);
		if (node != null) {	
			try {			
				return this.graphDb.getNodeById(node.getId());
			} catch (Exception ex) {
				throw new IllegalStateException(ex);
			}			
		}
		return null;
	}

	public Integer getNumberOfNodes () {
		if (tx == null){
			tx = graphDb.beginTx();
		}
		try {
			int numberOfNodes = Iterables.size((this.graphDb).getAllNodes());
		    if (++numOperations % BATCH_SIZE == 0) {
		        tx.success();
		        tx.close();
		        tx = graphDb.beginTx();
		    }	
			return numberOfNodes;
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
	}
	
	// METHODS RELATIONSHIPS OPERATIONS

	public static enum RelTypes implements RelationshipType {
		INFERRED_CHILD
	}

	public void createRelationship(Node orgNode, Node dstNode, RelationshipType type) {
		
		try {	
			if (tx == null){
				tx = graphDb.beginTx();
			}
			IndexManager index = graphDb.index();
			if ( ! index.existsForNodes( type.toString() ) ) {
				index.forRelationships( type.toString());
			}
			
			List<Relationship> relationships = this.mapRelationships.get(orgNode);
			
			if ( relationships == null ) {
				relationships = new ArrayList<Relationship>();
				Relationship rel = orgNode.createRelationshipTo(dstNode, type);
				relationships.add(rel);
				this.mapRelationships.put(orgNode, relationships);					
			} else {
				boolean isDuplicated = false;
				for (Relationship relationship: relationships ) {					
					if ( relationship.getEndNode().getId() == dstNode.getId() && relationship.getType().equals(type) ) {
						isDuplicated = true;
						break;
					}
				}

				if ( !isDuplicated ) {
					Relationship rel = orgNode.createRelationshipTo(dstNode, type);	
					relationships.add(rel);
				}
			}				
			
		    if (++numOperations % BATCH_SIZE == 0) {
		        tx.success();
		        tx.close();
		        tx = graphDb.beginTx();
		    }	
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}		
	}
	
	public void createRelationship2(Node orgNode, Node dstNode, RelationshipType type, String axiom) {
		
		try {	
			if (tx == null){
				tx = graphDb.beginTx();
			}
			IndexManager index = graphDb.index();
			if ( ! index.existsForNodes( type.toString() ) ) {
				index.forRelationships( type.toString());
			}			
					
			Relationship rel = orgNode.createRelationshipTo(dstNode, type);

			rel.setProperty("axiom", axiom);
						
		    if (++numOperations % BATCH_SIZE == 0) {
		        tx.success();
		        tx.close();
		        tx = graphDb.beginTx();
		    }	
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}		
	}	
	
	public Integer getNumberOfRelationships () {
		
		HashSet<Relationship> relationshipsSet = new HashSet<Relationship>();
		
		Transaction tx = null;
		try {
			tx = graphDb.beginTx();
			Iterable<Node> itNodes = getGraphDb().getAllNodes();
			for ( Node currentNode : itNodes ) {
				Iterable<Relationship> itRelationship = currentNode.getRelationships();
				for ( Relationship currentRelationship : itRelationship ) {
					relationshipsSet.add(currentRelationship);
				}
			}
			return relationshipsSet.size();
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
		finally {
			if ( tx != null ) tx.close();
		}
	}
	
	// GETTERS AND SETTERS
	
	public GraphDatabaseService getGraphDb() {
		return graphDb;
	}
	
	public HashMap<OWLClass, Node> getMapIriNode() {
		return mapIriNode;
	}
	
	public HashMap<Node, OWLClass> getMapNodeIri() {
		return mapNodeIri;
	}
	
	public void closeBatch(){
		tx.success();
		tx.close();
		tx = null;
	}
	
}
