package um.owlapi2neo4jgraph.reasoners;

import java.util.logging.Logger;

import org.semanticweb.HermiT.Configuration;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import au.csiro.snorocket.owlapi.SnorocketReasonerFactory;

public class OWLReasonerFactory {
	
	private enum ReasonerType {
		HERMIT, SNOROCKET
	}
	
	private static final ReasonerType SELECTED_REASONER = ReasonerType.SNOROCKET;	
	
	public static OWLReasoner createReasoner (OWLOntology ontology) throws Exception {
		// Create the reasoner
		Logger.getAnonymousLogger().info("Running " + SELECTED_REASONER + " reasoner");
						
		// Create Hermit classifier
		OWLReasoner owlReasoner  = null;
		switch (SELECTED_REASONER) {
			case HERMIT:
				Configuration config = new Configuration();
				owlReasoner = new Reasoner(config, ontology);
				break;
			case SNOROCKET:
				SnorocketReasonerFactory srf = new SnorocketReasonerFactory();
				owlReasoner = srf.createNonBufferingReasoner(ontology);
				break;
		}		
				
		// Classify
		Logger.getAnonymousLogger().info("Classifying");
		Logger.getAnonymousLogger().info("InferenceType="+InferenceType.CLASS_HIERARCHY+", "+InferenceType.DISJOINT_CLASSES);
		long initTime = System.currentTimeMillis();
		owlReasoner.precomputeInferences(
			InferenceType.CLASS_ASSERTIONS,
			InferenceType.CLASS_HIERARCHY,
			InferenceType.DATA_PROPERTY_ASSERTIONS,
			InferenceType.DATA_PROPERTY_HIERARCHY,
			InferenceType.DIFFERENT_INDIVIDUALS,
			InferenceType.DISJOINT_CLASSES,
			InferenceType.OBJECT_PROPERTY_ASSERTIONS,
			InferenceType.OBJECT_PROPERTY_HIERARCHY,
			InferenceType.SAME_INDIVIDUAL);
				
		// Check inconsistency
		Node<OWLClass> inconsistentClasses = owlReasoner.getUnsatisfiableClasses();
		if ( inconsistentClasses.getSize() > 1 ) {
			Logger.getAnonymousLogger().severe("The ontology has unsatisfiable classes so we stop the Oquare metrics calculation");
			for ( OWLClass unsatisfiableClasses : inconsistentClasses.getEntities()) {
				Logger.getAnonymousLogger().severe("\t"+unsatisfiableClasses);
			}
			System.exit(-1);
		}
				
		// Save the reasoning time
		long reasoningTime = System.currentTimeMillis() - initTime;
		Logger.getAnonymousLogger().info("Classifying time in "+reasoningTime+" msec");
	
		return owlReasoner;
		
	}
}
