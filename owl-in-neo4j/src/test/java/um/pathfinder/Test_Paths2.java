package um.pathfinder;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.validator.routines.UrlValidator;
import org.junit.Test;

import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;

import org.neo4j.helpers.collection.Iterables;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

import um.owlapi2neo4jgraph.Configuration;
import um.owlapi2neo4jgraph.Neo4jGraphedOntolgoyFactory;
import um.owlapi2neo4jgraph.Neo4jGraphedOntology;
import um.owlapi2neo4jgraph.Test_Neo4jGraphedOntology;
import um.owlapi2neo4jgraph.path.expanders.AscendInInferredHierachy;
import um.owlapi2neo4jgraph.reasoners.OWLReasonerFactory;
import um.owlapi2neo4jgraph.relationships.expanders.OwlClassExpander_InferredSubclasses;

public class Test_Paths2 {

	@Test
	public void test() throws Exception {
			
		String ontology = Test_Neo4jGraphedOntology.getResource(Configuration.getInstance().getValue(Test_Neo4jGraphedOntology.ONTOLOGY_FILE_KEY)).getPath();
		
		// Load the ontology with OWLApi
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology owlOntology;
		UrlValidator validator = new UrlValidator();
		if ( validator.isValid(ontology) ) {
			 owlOntology = manager.loadOntologyFromOntologyDocument(IRI.create(ontology));
		}
		else if ( (new File(ontology)).exists() ) {
			owlOntology = manager.loadOntologyFromOntologyDocument(new File(ontology));
		}
		else {
			throw new IllegalStateException("The ontology file format could not been recognised.");
		}		
				
		// Create the reasoner
		OWLReasoner owlReasoner = OWLReasonerFactory.createReasoner(owlOntology);
		
		// Create the expander
		OwlClassExpander_InferredSubclasses inferredModelExpander = new OwlClassExpander_InferredSubclasses(owlReasoner);
		
		PrefixManager pm = new DefaultPrefixManager("http://snomed.info/id/");
		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLClass owlClass = factory.getOWLClass("713459004", pm);
		
		Set<OWLClassAxiom> tempAx = owlOntology.getAxioms(owlClass);
		
		int i = 0;
		for (OWLClassAxiom ax : tempAx) {
			System.out.println("***");
			System.out.println("Axioma " + (i + 1) + " " + ax.getAxiomType());
			System.out.println(ax);
			
			System.out.println(" Axioma (signature)" + ax.getSignature());
			for (OWLClassExpression nce : ax.getNestedClassExpressions()) {
				if ( nce.getClassExpressionType() == ClassExpressionType.OBJECT_ALL_VALUES_FROM || 
					 nce.getClassExpressionType() == ClassExpressionType.OBJECT_SOME_VALUES_FROM ) {
//				if ( nce.getClassExpressionType() != ClassExpressionType.OWL_CLASS) {
					
					System.out.println("	### Class expression ### " + nce.toString());

					System.out.println("		Classes in signature");
					for (OWLClass oc : nce.getClassesInSignature()) {
						System.out.println("			" + oc.getIRI().getFragment());
						System.out.println("                " + owlReasoner.getSuperClasses(oc, true));
					}

					System.out.println("		Property");
					for (OWLObjectProperty op : nce.getObjectPropertiesInSignature()) {
						System.out.println("			" + op.getIRI().getFragment());
						System.out.println("			Rango: " + owlReasoner.getObjectPropertyRanges(op, true));
						
					}
				}
			}
			i++;
		}		
		
	}
	
	

}
