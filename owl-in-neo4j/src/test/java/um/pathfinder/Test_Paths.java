package um.pathfinder;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.validator.routines.UrlValidator;
import org.junit.Test;

import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;

import org.neo4j.helpers.collection.Iterables;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import um.owlapi2neo4jgraph.Configuration;
import um.owlapi2neo4jgraph.Neo4jGraphedOntolgoyFactory;
import um.owlapi2neo4jgraph.Neo4jGraphedOntology;
import um.owlapi2neo4jgraph.Test_Neo4jGraphedOntology;
import um.owlapi2neo4jgraph.path.expanders.AscendInInferredHierachy;
import um.owlapi2neo4jgraph.reasoners.OWLReasonerFactory;
import um.owlapi2neo4jgraph.relationships.expanders.OwlClassExpander_InferredSubclasses;

public class Test_Paths {

	@Test
	public void test() throws Exception {
			
		String ontology = Test_Neo4jGraphedOntology.getResource(Configuration.getInstance().getValue(Test_Neo4jGraphedOntology.ONTOLOGY_FILE_KEY)).getPath();
		
		// Load the ontology with OWLApi
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology owlOntology;
		UrlValidator validator = new UrlValidator();
		if ( validator.isValid(ontology) ) {
			 owlOntology = manager.loadOntologyFromOntologyDocument(IRI.create(ontology));
		}
		else if ( (new File(ontology)).exists() ) {
			owlOntology = manager.loadOntologyFromOntologyDocument(new File(ontology));
		}
		else {
			throw new IllegalStateException("The ontology file format could not been recognised.");
		}		
				
		// Create the reasoner
		OWLReasoner owlReasoner = OWLReasonerFactory.createReasoner(owlOntology);
		
		// Create the expander
		OwlClassExpander_InferredSubclasses inferredModelExpander = new OwlClassExpander_InferredSubclasses(owlReasoner);
		
		// Build the graph
		long iNeo4jCreationTime, fNeo4jCreationTime;
		iNeo4jCreationTime = System.currentTimeMillis();
		Neo4jGraphedOntology graph = new Neo4jGraphedOntology(owlOntology, inferredModelExpander);
		fNeo4jCreationTime = System.currentTimeMillis();
		Logger.getAnonymousLogger().info("Neo4jCreationTime: "+(fNeo4jCreationTime-iNeo4jCreationTime)+"msecs.");
				
//		// Create the path finder
//		PathFinder<Path> finder = AscendInInferredHierachy.getPathFinder(58);
//				
//		// Obtain the nods
//		String originNodeIri = Configuration.getInstance().getValue("originNode");
//		Node orgClassNode = graph.getFactory().getMapIriNode().get(owlOntology.getOWLOntologyManager().getOWLDataFactory().getOWLClass(IRI.create(originNodeIri)));
//		Node dstClassNode = graph.getFactory().getMapIriNode().get(owlOntology.getOWLOntologyManager().getOWLDataFactory().getOWLThing());
//		
//		// Calculate the path
//		Transaction tx = null;
//		try {
//			long initTime = System.currentTimeMillis();
//			tx = graph.getFactory().getGraphDb().beginTx();
//			// Calculate the path
//			Set<Path> paths = Iterables.asSet(finder.findAllPaths(orgClassNode, dstClassNode));
//			long endTime = System.currentTimeMillis();
//			Logger.getAnonymousLogger().info("Time"+(endTime-initTime)+"msec. Number of paths: "+paths.size());
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			throw new IllegalStateException(ex.getMessage());
//		}
//		finally {
//			tx.close();
//		}
//		// Use a Cypher Query
//		try {
//			tx = graph.getFactory().getGraphDb().beginTx();
//			long initTime = System.currentTimeMillis();
//			GraphDatabaseService graphDb = graph.getFactory().getGraphDb();
//			String query = "";//"MATCH (k)-[e*2..2]-(l) WHERE k.uri = \""+orgClassNode.getProperty("uri")+"\" AND l.name = \""+dstClassNode.getProperty("uri")+"\" RETURN k,e,l";			
//			
//			query += "START startnode=node("+orgClassNode.getId()+"), group = node("+dstClassNode.getId()+")\n";
//			query += "MATCH p=allShortestPaths(startnode<-[:"+Neo4jGraphedOntolgoyFactory.RelTypes.INFERRED_CHILD+"*..58]-group)\n";
//			query += "RETURN p\n";
//			
//			query = "start a=node("+dstClassNode.getId()+"), b=node("+orgClassNode.getId()+")\n";
//			query += "match p= (a)-[:"+Neo4jGraphedOntolgoyFactory.RelTypes.INFERRED_CHILD+"*1..58]->(b)\n";
//			//query += "where not(a-->b)\n";
//			query += "with p, relationships(p) as rcoll\n";
//			query += "return p \n";//, reduce(totalTime=0, x in rcoll: totalTime + x.time) as totalTime\n";
//			//query += "order by totalTime\n";
//			
//			Result rs = graph.getFactory().getGraphDb().execute(query);			
//			Set<Path> paths = new HashSet<Path>();
//
//			ResourceIterator<Object> columnAs = rs.columnAs("p");
//			int numRows = 0;
//
//			while(rs.hasNext())	{
//				Path pathObj = (Path) columnAs.next();				
//				paths.add(pathObj);
//				numRows++;
//			/*Node n = (Node)columnAs.next();
//			for (String key : n.getPropertyKeys()) {
//			System.out.println("{ " + key + " : " + n.getProperty(key)+ " } ");
//			}*/
//			}
//			long endTime = System.currentTimeMillis();
//			Logger.getAnonymousLogger().info("Cypher query time: "+(endTime-initTime)+" msec.");
//			Logger.getAnonymousLogger().info(paths.toString());
//			Logger.getAnonymousLogger().info("NumRows="+ numRows +" NumColumns="+rs.columns().size());
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			throw new IllegalStateException(ex.getMessage());
//		}
//		finally {
//			tx.close();
//		}
	}
	
	

}
