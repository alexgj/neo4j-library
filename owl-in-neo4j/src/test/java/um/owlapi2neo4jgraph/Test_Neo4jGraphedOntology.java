package um.owlapi2neo4jgraph;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Iterator;

import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Test_Neo4jGraphedOntology {

	public static final String ONTOLOGY_FILE_KEY = "ontologyFile";
	
	@Test
	public void test() throws OWLOntologyCreationException {
		
		// Create the factory
		Neo4jGraphedOntolgoyFactory factory = new Neo4jGraphedOntolgoyFactory();
		
		// Obtain the ontology
		OWLOntology pizzaOntology = getPizzaOntology();
		
		// Create nodes
		Integer counter = 1;
		for ( OWLClass owlClass : pizzaOntology.getClassesInSignature() ) {
			Node node = factory.createNode(owlClass);
			System.out.println(node+" "+node.getId());
			assertTrue(factory.getNumberOfNodes().intValue() == counter.intValue());
			counter++;
		}
		
		// Create a relationship
		Iterator<OWLClass> itPizzaClasses = pizzaOntology.getClassesInSignature().iterator();
		OWLClass orgClass = itPizzaClasses.next();
		OWLClass dstClass = itPizzaClasses.next();
		Node orgClassNode = factory.getNode(orgClass);
		Node dstClassNode = factory.getNode(dstClass);
		factory.createRelationship(orgClassNode, dstClassNode, Neo4jGraphedOntolgoyFactory.RelTypes.INFERRED_CHILD);
		Transaction tx = null;
		try {
			tx = factory.getGraphDb().beginTx();
			Iterable<Relationship> itRelationships = orgClassNode.getRelationships();
			Integer relationshipsCount = 0;
			for ( @SuppressWarnings("unused") Relationship currentRelationship : itRelationships ) {
				relationshipsCount++;
			}
			assertTrue(relationshipsCount == 1);
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
		finally {
			if ( tx != null ) tx.close();
		}
		assertTrue(factory.getNumberOfRelationships() == 1);
		
	}
	
	public static OWLOntology getPizzaOntology () throws OWLOntologyCreationException {
		// Load the file from the resources
		File pizzaOntology = getResource(Configuration.getInstance().getValue(Test_Neo4jGraphedOntology.ONTOLOGY_FILE_KEY));
		
		// Load the ontology with OWLApi
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		return manager.loadOntologyFromOntologyDocument(pizzaOntology);
	}

	public static File getResource(String resourceRelativePath) {

		// STEP 1: load the resource as an inputStream
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream rs = classloader.getResourceAsStream(resourceRelativePath);

		// STEP 2: copy the input stream as a temp file and return it
		if (rs != null) {
			return inputStream2TmpFile(rs);
		} else {
			return null;
		}

	}
	
	private static File inputStream2TmpFile(InputStream is) {
		try {
			// STEP 1: create the tmp file
			File tmpFile = File.createTempFile("tmp", "");
			
			// STEP 2: copy the input stream
			byte[] buffer = new byte[is.available()];
			is.read(buffer);
			
			OutputStream os = new FileOutputStream(tmpFile);
			os.write(buffer);
			os.close();
			
			// STEP 3: return the tmp file
			return tmpFile;
			
		} catch (IOException ex) {
			throw new IllegalStateException(ex.getMessage());
		}

	}
	
}
