package um.owlapi2neo4jgraph.path.expanders;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.helpers.collection.Iterables;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import um.owlapi2neo4jgraph.Neo4jGraphedOntology;
import um.owlapi2neo4jgraph.Test_Neo4jGraphedOntology;
import um.owlapi2neo4jgraph.reasoners.OWLReasonerFactory;
import um.owlapi2neo4jgraph.relationships.expanders.OwlClassExpander_InferredSubclasses;

public class Test_Paths_AscendInInferredHierarchy {

	private static final Integer MAX_DEPTH = 1000;
	
	private static final String ORG_NODE_URI = "http://www.co-ode.org/ontologies/pizza/pizza.owl#AnchoviesTopping";
	private static final String DST_NODE_URI = "http://www.co-ode.org/ontologies/pizza/pizza.owl#DomainConcept";
	
	private static final String PAHT_NODE_1 = "http://www.co-ode.org/ontologies/pizza/pizza.owl#AnchoviesTopping";
	private static final String PAHT_NODE_2 = "http://www.co-ode.org/ontologies/pizza/pizza.owl#FishTopping";
	private static final String PAHT_NODE_3 = "http://www.co-ode.org/ontologies/pizza/pizza.owl#PizzaTopping";
	private static final String PAHT_NODE_4 = "http://www.co-ode.org/ontologies/pizza/pizza.owl#Food";
	private static final String PAHT_NODE_5 = "http://www.co-ode.org/ontologies/pizza/pizza.owl#DomainConcept";

	@Test
	public void test() throws Exception {

		// Obtain the pizza ontology
		OWLOntology ontology = Test_Neo4jGraphedOntology.getPizzaOntology();
				
		// Reason the ontology 
		OWLReasoner reasoner = OWLReasonerFactory.createReasoner(ontology);
			
		// Create the expander for building the inferred model
		OwlClassExpander_InferredSubclasses inferredModelExpander = new OwlClassExpander_InferredSubclasses(reasoner);
				
		// Create a OwlApi2Neo4j graph from the ontology using inferred relationships
		Neo4jGraphedOntology graph = new Neo4jGraphedOntology(ontology, inferredModelExpander);
		
		// Create a path finder that expand by super class patterns
		PathFinder<Path> finder = AscendInInferredHierachy.getPathFinder(MAX_DEPTH);

		// Obtain two nodes
		OWLClass owlClassOrg = ontology.getOWLOntologyManager().getOWLDataFactory().getOWLClass(IRI.create(ORG_NODE_URI));
		Node orgNode = graph.getFactory().createNode(owlClassOrg);
		OWLClass owlClassDst = ontology.getOWLOntologyManager().getOWLDataFactory().getOWLClass(IRI.create(DST_NODE_URI));
		Node dstNode = graph.getFactory().createNode(owlClassDst);
		assertTrue(orgNode != null);
		assertTrue(dstNode != null);

		// Calculate the path
		GraphDatabaseService graphDb = graph.getFactory().getGraphDb();
		Transaction tx = null;
		try {
			tx = graphDb.beginTx();					
			// Calculate the path
			Set<Path> paths = Iterables.asSet(finder.findAllPaths(orgNode, dstNode));
			assertTrue(paths.size() == 1);
			// Check that the path is correct
			List<Relationship> pathRelationships = Iterables.asList(paths.iterator().next().relationships());
			assertTrue(pathRelationships.size() == 4);
			assertTrue(pathRelationships.get(0).getStartNode().getProperty("uri").toString().contains(PAHT_NODE_2));
			assertTrue(pathRelationships.get(0).getEndNode().getProperty("uri").toString().contains(PAHT_NODE_1));
			assertTrue(pathRelationships.get(0).getType().name().compareTo("INFERRED_CHILD") == 0 );
			assertTrue(pathRelationships.get(1).getStartNode().getProperty("uri").toString().contains(PAHT_NODE_3));
			assertTrue(pathRelationships.get(1).getEndNode().getProperty("uri").toString().contains(PAHT_NODE_2));
			assertTrue(pathRelationships.get(1).getType().name().compareTo("INFERRED_CHILD") == 0 );
			assertTrue(pathRelationships.get(2).getStartNode().getProperty("uri").toString().contains(PAHT_NODE_4));
			assertTrue(pathRelationships.get(2).getEndNode().getProperty("uri").toString().contains(PAHT_NODE_3));
			assertTrue(pathRelationships.get(2).getType().name().compareTo("INFERRED_CHILD") == 0 );
			assertTrue(pathRelationships.get(3).getStartNode().getProperty("uri").toString().contains(PAHT_NODE_5));
			assertTrue(pathRelationships.get(3).getEndNode().getProperty("uri").toString().contains(PAHT_NODE_4));
			assertTrue(pathRelationships.get(3).getType().name().compareTo("INFERRED_CHILD") == 0 );			
		} catch (Exception ex) {
			assertTrue(false);
		}
		finally {
			if ( tx != null ) tx.close();
		}

	}

}
