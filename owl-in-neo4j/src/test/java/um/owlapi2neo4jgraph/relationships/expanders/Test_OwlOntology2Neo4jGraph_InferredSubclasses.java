package um.owlapi2neo4jgraph.relationships.expanders;

import static org.junit.Assert.assertTrue;


import org.junit.Test;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import um.owlapi2neo4jgraph.Neo4jGraphedOntology;
import um.owlapi2neo4jgraph.Test_Neo4jGraphedOntology;
import um.owlapi2neo4jgraph.reasoners.OWLReasonerFactory;

public class Test_OwlOntology2Neo4jGraph_InferredSubclasses {

	private static final int SNOROCKET_EXPECTED_NUMBER_OF_NODES = 101;
	private static final int SNOROCKET_EXPECTED_NUMBER_OF_RELATIONSHPS = 217;
	
	@Test
	public void test() throws Exception {
		
		// Obtain the pizza ontology
		OWLOntology pizzaOntology = Test_Neo4jGraphedOntology.getPizzaOntology();
		
		// Reason the ontology 
		OWLReasoner reasoner = OWLReasonerFactory.createReasoner(pizzaOntology);
		
		// Create the expander for building the inferred model
		OwlClassExpander_InferredSubclasses inferredModelExpander = new OwlClassExpander_InferredSubclasses(reasoner);
		
		// Create a OwlApi2Neo4jReasoner
		Neo4jGraphedOntology graph = new Neo4jGraphedOntology(pizzaOntology, inferredModelExpander);
		assertTrue(graph.getFactory().getNumberOfNodes() == SNOROCKET_EXPECTED_NUMBER_OF_NODES);
		assertTrue(graph.getFactory().getNumberOfRelationships() == SNOROCKET_EXPECTED_NUMBER_OF_RELATIONSHPS);
		
	}	
}
